package com.silbury.risingking.controllers;

import com.silbury.risingking.configs.VaultConfig;
import com.silbury.risingking.models.AddRecordRequest;
import com.silbury.risingking.models.LoginRequest;
import com.silbury.risingking.models.RecordResponse;
import com.silbury.risingking.services.LoginService;
import com.silbury.risingking.services.RecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@Slf4j
public class MessageController {
    private final LoginService loginService;
    private final RecordService recordService;
    private final VaultConfig vaultConfig;
    private final VaultTemplate vaultTemplate;

    public MessageController(LoginService loginService, RecordService recordService, VaultConfig vaultConfig, VaultTemplate vaultTemplate) {
        this.loginService = loginService;
        this.recordService = recordService;
        this.vaultConfig = vaultConfig;
        this.vaultTemplate = vaultTemplate;
    }

    @GetMapping("records")
    public ResponseEntity<?> getRecords(@RequestParam String userId) {
        return new ResponseEntity<>(recordService.getRecords(userId), HttpStatus.OK);
    }

    @PostMapping(value = "record/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addRecord(@RequestBody AddRecordRequest addRecordRequest) {
        if (recordService.addRecord(addRecordRequest)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @PostMapping("login")
    public ResponseEntity<?> login(@RequestBody LoginRequest loginRequest) {

        return new ResponseEntity<>(loginService.login(loginRequest.getUserName(), loginRequest.getPassword()), HttpStatus.OK);
    }

    @GetMapping("grouprecords")
    public ResponseEntity<?> groupMessages(@RequestParam String coachId) {
        log.info("Getting record for coach "+ coachId);
        List<RecordResponse> records = recordService.getGroupRecords(coachId);
        log.info("Returning record for coach "+ coachId);
        return new ResponseEntity<>(records, HttpStatus.OK);
    }
}

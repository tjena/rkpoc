package com.silbury.risingking.repositories;

import com.silbury.risingking.entities.CoachAccess;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.stream.Stream;

public interface CoachAccessRepository extends JpaRepository<CoachAccess, String> {
    Stream<CoachAccess> findAllByCoachId(String coachId);
}

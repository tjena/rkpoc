package com.silbury.risingking.repositories;

import com.silbury.risingking.entities.Record;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.stream.Stream;

public interface RecordRepository extends JpaRepository<Record, String> {
    Stream<Record> findAllByUserId(String id);
}

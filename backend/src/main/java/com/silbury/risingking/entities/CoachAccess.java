package com.silbury.risingking.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@IdClass(CoachAccess.PK.class)
@Table(name = "SB_COACH_ACCESS")
public class CoachAccess {
    @Id
    @Column(name = "COACH_ID", length = 20)
    private String coachId;

    @Id
    @Column(name = "USER_ID", length = 20)
    private String userId;

    @Getter
    @Setter
    static class PK implements Serializable {
        private static final long serialVersionUID = 6230633356800533450L;

        private String coachId;
        private String userId;
    }
}

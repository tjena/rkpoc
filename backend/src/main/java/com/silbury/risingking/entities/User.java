package com.silbury.risingking.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "SB_USER")
public class User {
    @Id
    @Column(name = "USER_ID", length = 20)
    private String userId;

    @Column(name = "COUNTRY_CODE", length = 3)
    private String countryCode;

    @Column(name = "EMAIL", length = 40)
    private String email;

    @Column(name = "PASSWORD", length = 80)
    private String password;

    @Column(name = "SALT", length = 45)
    private String salt;

    @Column(name = "STATUS", length = 6)
    private String status;
}

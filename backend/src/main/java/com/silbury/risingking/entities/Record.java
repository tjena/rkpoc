package com.silbury.risingking.entities;

import com.silbury.risingking.converters.DataConverter;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "SB_RECORD")
public class Record {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MSG_ID", length = 20)
    private String msgId;

    @Column(name = "USER_ID", length = 2048)
    private String userId;

    @Column(name = "SECURE_MESSAGE_01", length = 2048)
    @Convert(converter = DataConverter.class)
    private String secureMessage01;

    @Column(name = "SECURE_MESSAGE_02", length = 2048)
    @Convert(converter = DataConverter.class)
    private String secureMessage02;

    @Column(name = "SECURE_MESSAGE_03", length = 2048)
    @Convert(converter = DataConverter.class)
    private String secureMessage03;

    @Column(name = "SECURE_MESSAGE_04", length = 2048)
    @Convert(converter = DataConverter.class)
    private String secureMessage04;

    @Column(name = "SECURE_MESSAGE_05", length = 2048)
    @Convert(converter = DataConverter.class)
    private String secureMessage05;

}

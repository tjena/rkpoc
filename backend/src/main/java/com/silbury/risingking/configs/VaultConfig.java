package com.silbury.risingking.configs;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("example")
@Data
public class VaultConfig {
    private String username;
}

package com.silbury.risingking.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class RecordResponse {
    private String decryptedContent;
}

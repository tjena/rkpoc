package com.silbury.risingking.models;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AddRecordRequest {
    private String userId;
    private Integer randomKeySize;
}

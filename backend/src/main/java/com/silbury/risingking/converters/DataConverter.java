package com.silbury.risingking.converters;

import com.silbury.risingking.security.encryptors.DataEncryptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.AttributeConverter;

@Service
@Slf4j
public class DataConverter implements AttributeConverter<String, String> {

    private final DataEncryptor dataEncryptor;

    public DataConverter(DataEncryptor dataEncryptor) {
        this.dataEncryptor = dataEncryptor;
    }

    @Override
    public String convertToDatabaseColumn(String attribute) {
        return dataEncryptor.encrypt(attribute);
    }

    @Override
    public String convertToEntityAttribute(String dbData) {
        return dataEncryptor.decrypt(dbData);
    }
}

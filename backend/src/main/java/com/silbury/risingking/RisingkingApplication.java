package com.silbury.risingking;

import com.silbury.risingking.configs.VaultConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties(VaultConfig.class)
@Slf4j
@SpringBootApplication
public class RisingkingApplication {

    public static void main(String[] args) {
        SpringApplication.run(RisingkingApplication.class, args);
    }

}

package com.silbury.risingking.vault;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.vault.authentication.TokenAuthentication;
import org.springframework.vault.client.VaultEndpoint;
import org.springframework.vault.core.VaultKeyValueOperationsSupport;
import org.springframework.vault.core.VaultSysOperations;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.vault.core.VaultTransitOperations;
import org.springframework.vault.support.VaultMount;
import org.springframework.vault.support.VaultResponse;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class VaultService {
//    private static final String secretKey = "$ji*)@4><OR%#@G^3qQ#$45$2%$56@F@VI@$#$@#%$YH89%&N^UKM&<U^KM>?K$";
//    private static final String secretKeySimple = "998cdff1fe3217e8";

    private VaultTemplate vaultTemplate;
    private VaultTransitOperations transitOperations;

    public List<String> getGroupsForUser(String userId) {
        VaultResponse response = vaultTemplate
                .opsForKeyValue("secret", VaultKeyValueOperationsSupport.KeyValueBackend.KV_2).get("rkconfig");
        List<String> userIds = null;
        try {
            userIds = Arrays.asList(response.getData().get(userId).toString().split(","));
        } catch (Exception e) {
            log.error("Unable to fetch userIds from");
        }

        return userIds;
    }

    public void mountKey() {
        VaultTransitOperations transitOperations = vaultTemplate.opsForTransit();
        VaultSysOperations sysOperations = vaultTemplate.opsForSys();
        if (!sysOperations.getMounts().containsKey("transit/")) {
            sysOperations.mount("transit", VaultMount.create("transit"));
        }
        transitOperations.rotate("user_02");
    }

    public String encrypt(String plainText, String userId) {
        return transitOperations.encrypt(userId, plainText);
    }

    public String decrypt(String encryptedText, String userId) {
        return transitOperations.decrypt(userId, encryptedText);
    }

    public void updateTransitOps() {
        try {
            URI uri = new URI("http://192.168.99.102:8200");
            VaultEndpoint endpoint = VaultEndpoint.from(uri);
//            vaultTemplate = new VaultTemplate(endpoint, new TokenAuthentication("s.pkIhrGGFlWprqrPiehSrGKWR"));//user01
            vaultTemplate = new VaultTemplate(endpoint, new TokenAuthentication("s.Epd5ZScOozX3ypChJfNBZarg"));//root
//            vaultTemplate = new VaultTemplate(endpoint, new TokenAuthentication("s.EX0qy7GK3VGiPBmkzwSQboQ4"));//coach01
            transitOperations = vaultTemplate.opsForTransit("transitkey");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}

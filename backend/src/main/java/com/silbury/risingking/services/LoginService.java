package com.silbury.risingking.services;

public interface LoginService {
    String login(String userId, String password);
}

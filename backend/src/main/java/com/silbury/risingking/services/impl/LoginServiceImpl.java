package com.silbury.risingking.services.impl;

import com.silbury.risingking.entities.User;
import com.silbury.risingking.repositories.UserRepository;
import com.silbury.risingking.security.encryptors.JWTEncryptor;
import com.silbury.risingking.services.LoginService;
import com.silbury.risingking.vault.VaultService;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import static org.apache.commons.codec.digest.MessageDigestAlgorithms.SHA3_256;


@Service
public class LoginServiceImpl implements LoginService {
    private static final Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);
    private final UserRepository userRepository;
    private final JWTEncryptor jwtEncryptor;

    private final VaultService vaultService;

    public LoginServiceImpl(UserRepository userRepository, JWTEncryptor jwtEncryptor, VaultService vaultService) {
        this.userRepository = userRepository;
        this.jwtEncryptor = jwtEncryptor;
        this.vaultService = vaultService;
    }

    @Override
    @Transactional
    public String login(String userId, String password) {
        vaultService.mountKey();
        User user = this.userRepository.findByUserId(userId);
        String combinedPass = password + user.getSalt();
        String digestString = new DigestUtils(SHA3_256).digestAsHex(combinedPass);
        String token = null;
        if (digestString.equals(user.getPassword())) {
            token = jwtEncryptor.generateToken(userId, null);
        }
        logger.info(digestString);
        return token;
    }
}

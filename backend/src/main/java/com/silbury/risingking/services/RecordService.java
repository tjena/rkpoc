package com.silbury.risingking.services;

import com.silbury.risingking.models.AddRecordRequest;
import com.silbury.risingking.models.RecordResponse;

import java.util.List;

public interface RecordService {
    List<RecordResponse> getRecords(String id);

    boolean addRecord(AddRecordRequest addRecordRequest);

    List<RecordResponse> getGroupRecords(String userId);
}

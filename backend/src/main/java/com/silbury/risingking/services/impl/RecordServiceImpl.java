package com.silbury.risingking.services.impl;

import com.silbury.risingking.entities.Record;
import com.silbury.risingking.models.AddRecordRequest;
import com.silbury.risingking.models.RecordResponse;
import com.silbury.risingking.repositories.CoachAccessRepository;
import com.silbury.risingking.repositories.RecordRepository;
import com.silbury.risingking.security.encryptors.DataEncryptor;
import com.silbury.risingking.services.RecordService;
import com.silbury.risingking.vault.VaultService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class RecordServiceImpl implements RecordService {

    private final RecordRepository recordRepository;
    private final VaultService vaultService;
    private final CoachAccessRepository coachAccessRepository;

    private final DataEncryptor dataEncryptor;
    private int counter = 0;

    public RecordServiceImpl(RecordRepository recordRepository, VaultService vaultService, DataEncryptor dataEncryptor, CoachAccessRepository coachAccessRepository) {
        this.recordRepository = recordRepository;
        this.vaultService = vaultService;
        this.dataEncryptor = dataEncryptor;
        this.coachAccessRepository = coachAccessRepository;
    }

    @Override
    @Transactional
    public List<RecordResponse> getRecords(String userId) {
        dataEncryptor.setEncryptor(userId);
        return this.recordRepository.findAllByUserId(userId)
                .map(r -> new RecordResponse(r.toString()))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public boolean addRecord(AddRecordRequest r) {
        dataEncryptor.setEncryptor(r.getUserId());
        Record record = new Record();
        String userId = r.getUserId();
        record.setUserId(userId);
        String randomString = RandomStringUtils.randomAlphanumeric(r.getRandomKeySize());
        record.setSecureMessage01(userId + " : " + randomString + " counter : " + counter++);
        record.setSecureMessage02(userId + " : " + randomString + " counter : " + counter++);
        record.setSecureMessage03(userId + " : " + randomString + " counter : " + counter++);
        record.setSecureMessage04(userId + " : " + randomString + " counter : " + counter++);
        record.setSecureMessage05(userId + " : " + randomString + " counter : " + counter++);

        recordRepository.save(record);
        return true;
    }

    @Override
    @Transactional
    public List<RecordResponse> getGroupRecords(String coachId) {
        return coachAccessRepository.findAllByCoachId(coachId).flatMap(uid -> {
            dataEncryptor.setEncryptor(uid.getUserId());
            return recordRepository.findAllByUserId(uid.getUserId())
                    .map(r -> new RecordResponse(r.toString()));
        }).collect(Collectors.toList());
    }
}

package com.silbury.risingking.security.encryptors;

import com.silbury.risingking.vault.VaultService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class DataEncryptor {

    private final VaultService vaultService;

    private String userId;

    public DataEncryptor(VaultService vaultService) {
        this.vaultService = vaultService;
    }

    public String encrypt(String plainText) {
        return vaultService.encrypt(plainText, userId);
    }

    public String decrypt(String encryptedText) {
        return vaultService.decrypt(encryptedText, userId);
    }

    public void setEncryptor(String userId) {
        this.vaultService.updateTransitOps();
        this.userId = userId;
    }
}

package com.silbury.risingking.security;

//@EnableWebSecurity
public class SecurityConfiguration {
//    extends WebSecurityConfigurerAdapter {
//
//    private final BaseAuthenticationFilter authenticationFilter;
//    private final BaseAuthenticationEntryPoint authenticationEntryPoint;
//
//    @Value("#{'${sb.unprotected.url}'.split(',')}")
//    private String[] unprotectedUrl;
//
//    public SecurityConfiguration(BaseAuthenticationFilter authenticationFilter,
//                                 BaseAuthenticationEntryPoint authenticationEntryPoint) {
//        this.authenticationFilter = authenticationFilter;
//        this.authenticationEntryPoint = authenticationEntryPoint;
//    }
//
//    @Override
//    public void configure(WebSecurity web) {
//        web.ignoring().antMatchers(unprotectedUrl);
//    }
//
//    @Override
//    protected void configure(HttpSecurity httpSecurity) throws Exception {
//        httpSecurity
//                .csrf().disable()
//                .authorizeRequests().antMatchers(unprotectedUrl).permitAll()
//                .anyRequest().authenticated().and()
//                .exceptionHandling().authenticationEntryPoint(authenticationEntryPoint).and()
//                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
//        httpSecurity.addFilterBefore(authenticationFilter, UsernamePasswordAuthenticationFilter.class).headers().cacheControl();
//    }
//}
//
//@Component
//class BaseAuthenticationEntryPoint implements AuthenticationEntryPoint {
//    @Override
//    public void commence(HttpServletRequest request, HttpServletResponse response,
//                         AuthenticationException
//                                 authException) throws IOException {
//        response.sendError(HttpServletResponse.SC_FORBIDDEN, "Forbidden user");
//    }
//}
//
//@Component
//class BaseAuthenticationFilter extends GenericFilterBean {
//    @Value("${sb.jwt.token.header}")
//    private String header;
//
//    @Value("#{'${sb.unprotected.url}'.split(',')}")
//    private String[] unprotectedUrl;
//
//    private final JWTEncryptor jwtEncryptor;
//
//    public BaseAuthenticationFilter(JWTEncryptor jwtEncryptor) {
//        this.jwtEncryptor = jwtEncryptor;
//    }
//
//    @Override
//    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
//            throws IOException, ServletException {
//
//        SecurityContextHolder.getContext()
//                .setAuthentication(getAuthentication((HttpServletRequest) req));
//        chain.doFilter(req, res);
//    }
//
//    private Authentication getAuthentication(HttpServletRequest request) {
//        boolean isUnprotected = Arrays.stream(this.unprotectedUrl)
//                .anyMatch(str -> str.equals(request.getRequestURI()));
//        String token = request.getHeader(header);
//        if (isUnprotected || StringUtils.isEmpty(token)) {
//            return null;
//        }
//        Claims claims = jwtEncryptor.validateToken(token);
//        List<String> groups = claims.get("groups", List.class);
//        String userId = claims.get("name", String.class);
//
//        AuthorizedUser authUser = new AuthorizedUser(groups, userId);
//        return new BaseAuthentication(authUser);
//    }
//}
//
//class BaseAuthentication implements Authentication {
//    private static final long serialVersionUID = -6237460874337922185L;
//
//    private final AuthorizedUser authUser;
//    private final List<String> groups;
//
//    BaseAuthentication(AuthorizedUser authUser) {
//        this.authUser = authUser;
//        this.groups = authUser.getGroups();
//    }
//
//    @Override
//    public List<GrantedAuthority> getAuthorities() {
//        return null;
//    }
//
//    @Override
//    public List<String> getCredentials() {
//        return this.groups;
//    }
//
//    @Override
//    public Object getDetails() {
//        return null;
//    }
//
//    @Override
//    public AuthorizedUser getPrincipal() {
//        return authUser;
//    }
//
//    @Override
//    public boolean isAuthenticated() {
//        return true;
//    }
//
//    @Override
//    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
//
//    }
//
//    @Override
//    public boolean equals(Object another) {
//        return false;
//    }
//
//
//    @Override
//    public int hashCode() {
//        return 0;
//    }
//
//    @Override
//    public String getName() {
//        return authUser.getUserId();
//    }
}

package com.silbury.risingking.security.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;
import java.util.List;

@Getter
@AllArgsConstructor
public class AuthorizedUser implements Serializable {
    private static final long serialVersionUID = -7337706437930857720L;
    private List<String> groups;
    private String userId;
}

### Setup Vagrant
1. go to the setup folder and run `vagrant up` 
    - with this mysql db is installed
    - it creates a db schema `risingking`, user `rkuser` and password `welcome1`
    - ip of the vagrant is 192.168.99.102
    - url to connect to db is `jdbc:mysql://192.168.99.102:3306/risingking`
2. only hello world angular frontend is setup (just ignore it)
3. for backend spring boot application have some basic implementations for poc
      
### Installing vault
1. download vault and extract the executable
2. copy it to /usr/bin
3. create folder /etc/vault for vault config files
4. create folder /opt/vault-data to keep secrets
5. create /logs/vault/ to keep the vault logs
6. create vault config file /etc/vault/config.json with content

    ```bash{
      "listener":[{
              "tcp":{
                      "address":"0.0.0.0:8200",
                      "tls_disable":1
              }
      }],
      "api_addr":"http://192.168.99.1:8200",
      "storage":{
              "file":{
                      "path":"/opt/vault-data"
              }
      },
      "max_lease_ttl":"10h",
      "default_lease_ttl":"10h",
      "ui":true
      }
    ```

   - the api_address is the ip address of your local system for the host-only network used by vagrant
   - e.g. ipconfig will give the result 
        
    ```bash
    Ethernet adapter VirtualBox Host-Only Network #2:
       
          Connection-specific DNS Suffix  . :
          Link-local IPv6 Address . . . . . : fe80::d0d6:3864:1786:6229%12
          IPv4 Address. . . . . . . . . . . : 192.168.99.1
          Subnet Mask . . . . . . . . . . . : 255.255.255.0
          Default Gateway . . . . . . . . . :
    ```
     
7. create a vault service /etc/systemd/system/vault.service with the content 
    
    ```bash
    [Unit]
    Description=vault service
    Requires=network-online.target
    After=network-online.target
    ConditionFileNotEmpty=/etc/vault/config.json
    
    [Service]
    EnvironmentFile=-/etc/sysconfig/vault
    Environment=GOMAXPROCS=2
    Restart=on-failure
    ExecStart=/usr/bin/vault server -config=/etc/vault/config.json
    StandardOutput=/logs/vault/output.log
    StandardError=/logs/vault/error.log
    LimitMEMLOCK=infinity
    ExecReload=/bin/kill -HUP $MAINPID
    KillSignal=SIGTERM
    
    [Install]
    WantedBy=multi-user.target
    
    ```

8. start and check the status 
    - systemctl start vault.service
    - systemctl status vault.service
9. open the vault in browser and in the first time login it will guide you to generate key to unseal the vault and root keys. 

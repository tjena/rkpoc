#!/usr/bin/env bash
apt-get update
apt-get upgrade

echo "setup mysql"
echo "mysql-server-5.7 mysql-server/root_password password root" | sudo debconf-set-selections
echo "mysql-server-5.7 mysql-server/root_password_again password root" | sudo debconf-set-selections

sudo apt-get -y install mysql-server default-libmysqlclient-dev
sed -ie 's/127.0.0.1/192.168.99.102/g' /etc/mysql/mysql.conf.d/mysqld.cnf

#create msql users for different schemas
mysql -uroot -proot -e "CREATE USER 'rkuser'@'%' IDENTIFIED BY 'welcome1';"
mysql -uroot -proot -e "GRANT ALL ON *.* TO 'rkuser'@'%' WITH GRANT OPTION;";

#create schemas
mysql -uroot -proot -e "CREATE SCHEMA risingking"

apt-get update
apt-get upgrade
apt-get autoremove --purge
apt-get autoclean

echo "####################################################"
echo "####################################################"
echo "####################################################"
echo "####################################################"
echo "setup is complete"
echo "####################################################"
echo "Reboot is needed."
echo "Reboot virtual box using the commands, vagrant halt and vagrant up"
echo "####################################################"

